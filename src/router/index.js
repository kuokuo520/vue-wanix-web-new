import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Download from "@/views/Download";
import Buy from "@/views/Buy"
import Affiliate from "@/views/Affiliate";
import Game from "@/views/Game";
import Education from "@/views/Education";
import Enterprise from "@/views/Enterprise";
import Support from "@/views/Support";
import SupportMalus from "@/views/SupportMalus";
import ForStudents from "@/views/ForStudents";
import ForLife from "@/views/ForLife";
import ForTravel from "@/views/ForTravel";
import Login from "@/views/Login";
import Dashboard from "@/views/Dashboard";
import Index from '@/views/Index';
import Signup from "@/views/Signup";
import Chrome from "@/views/Chrome";
import Ios from "@/views/Ios";
import Android from "@/views/Android";
import Mac from "@/views/Mac";
import Windows from "@/views/Windows";
import Tv from "@/views/Tv";
import News from "@/views/News";
import Changelog from "@/views/Changelog";
import Status from "@/views/Status";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Index',
        component: Index,
        children: [
            {
                path: '/',
                name: 'Home',
                component: Home
            },
            {
                path: '/download',
                name: 'Download',
                component: Download
            },
            {
                path: '/buy',
                name: 'Buy',
                component: Buy
            },
            {
                path: '/affiliate',
                name: 'Affiliate',
                component: Affiliate,
            },
            {
                path: '/game',
                name: 'Game',
                component: Game
            },
            {
                path: '/education',
                name: 'Education',
                component: Education
            },
            {
                path: '/enterprise',
                name: 'Enterprise',
                component: Enterprise
            },
            {
                path: '/support',
                name: 'Support',
                component: Support
            },
            {
                path: '/support/malus',
                name: 'SupportMalus',
                component: SupportMalus
            },
            {
                path: '/for/students',
                name: 'ForStudents',
                component: ForStudents
            },
            {
                path: '/for/life',
                name: 'ForLife',
                component: ForLife
            },
            {
                path: '/for/travel',
                name: 'ForTravel',
                component: ForTravel
            },
            {
                path: 'signup',
                name: 'Signup',
                component: Signup
            },
            {
                path: 'chrome',
                name: 'Chrome',
                component: Chrome
            },
            {
                path: 'ios',
                name: 'Ios',
                component: Ios
            },
            {
                path: 'android',
                name: 'Android',
                component: Android
            },
            {
                path: 'mac',
                name: 'Mac',
                component: Mac
            },
            {
                path: 'windows',
                name: 'Windows',
                component: Windows
            },
            {
                path: 'tv',
                name: 'Tv',
                component: Tv
            },
            {
                path: 'news',
                name: 'News',
                component: News
            },
            {
                path: 'changelog',
                name: 'Changelog',
                component: Changelog
            },
            {
                path: 'status',
                name: 'Status',
                component: Status
            }
        ]
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        children: [
            {
                path: 'login',
                name: 'Login',
                component: Login
            }
        ]
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
